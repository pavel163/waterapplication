package com.ebr163.waterapplication.util;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bakht on 24.02.2016.
 */
public class FontCache {

    public static final String ROBOTO_LIGHT_FILENAME = "Roboto-Light.ttf";
    public static final String ROBOTO_CONDENSED_FILENAME = "RobotoCondensed-Regular.ttf";
    public static final String ROBOTO_CONDENSED_BOLD_FILENAME = "RobotoCondensed-Bold.ttf";
    public static final String ROBOTO_CONDENSED_LIGHT_FILENAME = "RobotoCondensed-Light.ttf";
    public static final String ROBOTO_SLAB_FILENAME = "RobotoSlab-Regular.ttf";

    public static final String ROBOTO_LIGHT_NATIVE_FONT_FAMILY = "sans-serif-light";
    public static final String ROBOTO_CONDENSED_NATIVE_FONT_FAMILY = "sans-serif-condensed";

    private static Map<String, Typeface> fontCache = new HashMap<>();

    public static Typeface getTypeface(String fontname, Context context) {
        Typeface typeface = fontCache.get(fontname);

        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/"+fontname);
            } catch (Exception e) {
                return null;
            }

            fontCache.put(fontname, typeface);
        }

        return typeface;
    }
}
