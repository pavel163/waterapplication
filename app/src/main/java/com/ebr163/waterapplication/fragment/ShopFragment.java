package com.ebr163.waterapplication.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.CheckActivity;
import com.ebr163.waterapplication.adapter.ListProductAdapter;
import com.ebr163.waterapplication.event.CostEvent;
import com.ebr163.waterapplication.fragment.base.BaseFragmentWithList;
import com.ebr163.waterapplication.model.Product;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 28.02.2016.
 */
public class ShopFragment extends BaseFragmentWithList {

    private List<Product> products;

    private LinearLayout linearLayout;
    private Button button;
    private TextView summText;

    public static ShopFragment newInstance() {
        ShopFragment shopFragment = new ShopFragment();
        return shopFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        objectListener = (OnSetObjectListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.fragment_list);
        EventBus.getDefault().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (products == null) {
            objectListener.setProducts();
        } else {
            setupRecyclerView();
        }

        return view;
    }

    @Override
    public void setupRecyclerView() {
        super.setupRecyclerView();
        linearLayout.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        ListProductAdapter adapter = new ListProductAdapter(products);
        recyclerView.setAdapter(adapter);
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    protected void initUI(View view) {
        super.initUI(view);
        linearLayout = (LinearLayout) view.findViewById(R.id.order_liner);
        button = (Button) view.findViewById(R.id.btn_order);
        button.setOnClickListener(this);
        summText = (TextView) view.findViewById(R.id.summ);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_order:
                Intent intent = new Intent(getActivity(), CheckActivity.class);
                intent.putExtra("sum", summText.getText().toString());
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(CostEvent event){
        int sum = Integer.parseInt(summText.getText().toString());
        if (event.isFlag()){
            sum = sum + Integer.parseInt(event.getData().toString());
        } else {
            sum = sum - Integer.parseInt(event.getData().toString());
        }
        summText.setText(String.valueOf(sum));
    }
}
