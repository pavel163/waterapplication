package com.ebr163.waterapplication.adapter.check;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.ui.SmartyTextView;
import com.ebr163.waterapplication.util.AppHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mac1 on 21.03.16.
 */
public class CheckAdapter extends BaseAdapter {

    private List<Map<String, Object>> products;
    private Context ctx;
    private LayoutInflater lInflater;

    public CheckAdapter(Context context, List<Map<String, Object>> products){
        this.products = products;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_check, parent, false);
        }

        ((SmartyTextView) view.findViewById(R.id.name)).setText(products.get(position).get(AppHelper.NAME).toString());
        ((SmartyTextView) view.findViewById(R.id.count)).setText("Колличество " + products.get(position).get(AppHelper.COUNT).toString());
        return view;
    }
}
