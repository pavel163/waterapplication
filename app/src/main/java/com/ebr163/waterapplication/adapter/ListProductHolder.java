package com.ebr163.waterapplication.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.DescriptionActivity;
import com.ebr163.waterapplication.event.CostEvent;
import com.ebr163.waterapplication.ui.SmartyImageView;
import com.ebr163.waterapplication.ui.SmartyTextView;
import com.ebr163.waterapplication.util.AppHelper;

import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * Created by Bakht on 28.02.2016.
 */
public class ListProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private SmartyImageView image;
    private SmartyTextView name;
    private SmartyTextView cost;
    private SmartyTextView count;
    private Button plus;
    private Button minus;
    private TextView id;

    public static ListProductHolder newInstance(View parent) {
        SmartyImageView image = (SmartyImageView) parent.findViewById(R.id.image);
        SmartyTextView name = (SmartyTextView) parent.findViewById(R.id.name);
        SmartyTextView cost = (SmartyTextView) parent.findViewById(R.id.cost);
        SmartyTextView count = (SmartyTextView) parent.findViewById(R.id.count);
        Button plus = (Button) parent.findViewById(R.id.btn_plus);
        Button minus = (Button) parent.findViewById(R.id.btn_minus);
        TextView id = (TextView) parent.findViewById(R.id.product_id);
        return new ListProductHolder(parent, image, name, cost, count, plus, minus, id);
    }

    public ListProductHolder(View itemView, SmartyImageView image, SmartyTextView name, SmartyTextView cost, SmartyTextView count, Button plus, Button minus, TextView id) {
        super(itemView);
        this.image = image;
        this.image.setOnClickListener(this);
        this.name = name;
        this.cost = cost;
        this.count = count;
        this.plus = plus;
        this.plus.setOnClickListener(this);
        this.minus = minus;
        this.minus.setOnClickListener(this);
        this.id = id;
    }

    public void setImage(String url){
        image.setTag("http://h90940.s04.test-hf.ru/apanel/assets/images/"+url);
        image.setImage("http://h90940.s04.test-hf.ru/apanel/assets/images/"+url);
    }

    public void setName(String name){
        this.name.setText(name);
    }

    public void setCost(String cost){
        this.cost.setText(cost);
    }

    public void setId(String id){
        this.id.setText(id);
    }

    @Override
    public void onClick(View v) {
        String id = this.id.getText().toString();
        Map<String, Pair<String, Integer>> map = AppHelper.listProducts;
        switch (v.getId()){
            case R.id.btn_plus:
                int p = Integer.parseInt(count.getText().toString())+1;
                count.setText(String.valueOf(p));
                if (map.containsKey(id)){
                    map.put(id, new Pair(name.getText().toString(), map.get(id).second+1));
                } else {
                    map.put(id, new Pair(name.getText().toString(), 1));
                }
                EventBus.getDefault().post(new CostEvent(cost.getText().toString(), true));
                break;
            case R.id.btn_minus:
                int m = Integer.parseInt(count.getText().toString());
                if (m - 1 >=0 ){
                    count.setText(String.valueOf(m-1));
                    if (map.containsKey(id)){
                        map.put(id, new Pair(name.getText().toString(), map.get(id).second-1));
                        EventBus.getDefault().post(new CostEvent(cost.getText().toString(), false));
                    }
                }
                if (m-1 == 0) {
                    if (map.containsKey(id)){
                        map.remove(id);
                    }
                }

                break;
            case R.id.image:
                Intent intent = new Intent(v.getContext(), DescriptionActivity.class);
                intent.putExtra("image", (String) image.getTag());
                intent.putExtra("id", id);
                intent.putExtra("name", name.getText().toString());
                v.getContext().startActivity(intent);
                break;
            default:
                break;
        }
    }
}
