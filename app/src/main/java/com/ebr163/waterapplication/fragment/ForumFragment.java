package com.ebr163.waterapplication.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.MessageActivity;
import com.ebr163.waterapplication.fragment.base.BaseFragment;
import com.google.android.gms.analytics.HitBuilders;

/**
 * Created by Bakht on 06.03.2016.
 */
public class ForumFragment extends BaseFragment implements View.OnClickListener {

    private EditText message;
    private Button sendBtn;
    private OnSendListener sendListener;

    public interface OnSendListener {
        void send(String message);
    }

    public static ForumFragment newInstance(){
        ForumFragment forumFragment = new ForumFragment();
        return forumFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        sendListener = (OnSendListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.fragment_forum);
        t.setScreenName("Экран обраной связи");
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    protected void initUI(View view){
        message = (EditText) view.findViewById(R.id.edit_comment);
        sendBtn = (Button) view.findViewById(R.id.send_comment);
        sendBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        sendListener.send(message.getText().toString());
    }
}
