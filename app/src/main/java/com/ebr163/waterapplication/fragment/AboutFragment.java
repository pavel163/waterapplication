package com.ebr163.waterapplication.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.fragment.base.BaseFragment;

/**
 * Created by Bakht on 05.04.2016.
 */
public class AboutFragment extends BaseFragment implements View.OnClickListener {

    public static AboutFragment newInstance(){
        return new AboutFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.fragment_about);
    }

    @Override
    protected void initUI(View view) {
        view.findViewById(R.id.phone).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+78633090575"));
        startActivity(intent);
    }
}
