package com.ebr163.waterapplication.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.ebr163.waterapplication.util.FontCache;


/**
 * Created by Bakht on 21.02.2016.
 */
public class SmartyTextView extends TextView {

    public SmartyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context, FontCache.ROBOTO_CONDENSED_NATIVE_FONT_FAMILY);
    }

    public SmartyTextView(Context context) {
        super(context);
        setFont(context, FontCache.ROBOTO_CONDENSED_NATIVE_FONT_FAMILY);

    }

    public SmartyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context, FontCache.ROBOTO_CONDENSED_NATIVE_FONT_FAMILY);
    }

    public void setFont(Context context, String font){
        Typeface customFont = FontCache.getTypeface(font, context);
        setTypeface(customFont);
    }
}
