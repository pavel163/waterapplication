package com.ebr163.waterapplication.event;

/**
 * Created by Bakht on 03.03.2016.
 */
public class BaseEvent {

    private Object data;

    public BaseEvent(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
