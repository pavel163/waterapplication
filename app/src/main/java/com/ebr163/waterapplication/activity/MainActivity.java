package com.ebr163.waterapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.ebr163.waterapplication.BaseApplication;
import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.base.BaseActivityWithFragment;
import com.ebr163.waterapplication.fragment.AboutFragment;
import com.ebr163.waterapplication.fragment.DeliveryFragment;
import com.ebr163.waterapplication.fragment.ForumFragment;
import com.ebr163.waterapplication.fragment.PriceFragment;
import com.ebr163.waterapplication.fragment.ProfFragment;
import com.ebr163.waterapplication.fragment.SaleDialog;
import com.ebr163.waterapplication.fragment.ShopFragment;
import com.ebr163.waterapplication.fragment.base.BaseFragment;
import com.ebr163.waterapplication.fragment.base.BaseFragmentWithList;
import com.ebr163.waterapplication.model.Product;
import com.ebr163.waterapplication.service.SettingsService;
import com.ebr163.waterapplication.util.AppHelper;
import com.ebr163.waterapplication.util.DataUtils;
import com.google.android.gms.analytics.HitBuilders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class MainActivity extends BaseActivityWithFragment implements NavigationView.OnNavigationItemSelectedListener,  ForumFragment.OnSendListener, BaseFragmentWithList.OnSetObjectListener {

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private int mCurrentSelectedPosition = 0;
    private ProgressDialog progressDialog;
    private static final String TAG_SHOP = "shop";
    private static final String TAG_PROFILE = "profile";
    private static final String TAG_CHAT = "chat";
    private static final String TAG_ABOUT = "about";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
        setupFragment(mCurrentSelectedPosition);
        if ("".equals(SettingsService.getString(this, SettingsService.FIRST_START))){
            DialogFragment dialog = new SaleDialog();
            dialog.show(fragment.getFragmentManager(), dialog.getClass().getName());
        }
        t.setScreenName("Экран со списком товаров");
        t.send(new HitBuilders.ScreenViewBuilder()
                .setCampaignParamsFromUrl(getIntent().getStringExtra("referrer"))
                .build());

        if (SettingsService.getData(this).first == 0){
            SettingsService.saveData(this, DataUtils.getCurrentDayAndMonth());
        }
    }

    @Override
    protected void initUI() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Отправка отзыва...");
        super.initUI();
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        setToolbarTitle("Товары");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();

        if (id == R.id.phone) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:+78633090575"));
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_shop) {
            clickMenuItem(0);
        } else if (id == R.id.nav_person) {
            clickMenuItem(1);
        } else if (id == R.id.nav_chat) {
            clickMenuItem(2);
        } else if (id == R.id.about) {
            clickMenuItem(3);
        } else if (id == R.id.price) {
            clickMenuItem(4);
        } else if (id == R.id.delivery) {
            clickMenuItem(5);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void clickMenuItem(int position) {
        mCurrentSelectedPosition = position;
        setupFragment(position);
    }

    private void setupFragment(int index) {
        String tag = null;
        switch (index) {
            case 0:
                tag = TAG_SHOP;
                fragment = (BaseFragment) fragmentManager.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = ShopFragment.newInstance();
                }
                setToolbarTitle("Товары");
                break;
            case 1:
                tag = TAG_PROFILE;
                fragment = (BaseFragment) fragmentManager.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = ProfFragment.newInstance();
                }
                setToolbarTitle("Профиль");
                AppHelper.listProducts.clear();
                break;
            case 2:
                tag = TAG_CHAT;
                fragment = (BaseFragment) fragmentManager.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = ForumFragment.newInstance();
                }
                setToolbarTitle("Отправить отзыв");
                AppHelper.listProducts.clear();
                break;
            case 3:
                tag = TAG_ABOUT;
                fragment = (BaseFragment) fragmentManager.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = AboutFragment.newInstance();
                }
                setToolbarTitle("О нас");
                AppHelper.listProducts.clear();
                break;
            case 4:
                tag = "price";
                fragment = (BaseFragment) fragmentManager.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = PriceFragment.newInstance();
                }
                setToolbarTitle("Оплата и цены");
                AppHelper.listProducts.clear();
                break;
            case 5:
                tag = "delivery";
                fragment = (BaseFragment) fragmentManager.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = DeliveryFragment.newInstance();
                }
                setToolbarTitle("Доставка");
                AppHelper.listProducts.clear();
                break;
            default:
                break;
        }
        fragmentManager.beginTransaction()
                .replace(R.id.main_fragment_container, fragment, tag)
                .commit();
    }

    @Override
    public void setProducts() {
        Action1<List<Product>> onNextAction = new Action1<List<Product>>() {
            @Override
            public void call(List<Product> s) {
                if (s != null) {
                    ((ShopFragment) fragment).setProducts(s);
                    ((ShopFragment) fragment).setupRecyclerView();
                    Log.d("Загрузка товаров", "Успех");
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                ((BaseFragmentWithList) fragment).setVisibleEmptyView(View.VISIBLE);
                ((BaseFragmentWithList) fragment).setVisibleProgress(View.GONE);
                Log.d("Загрузка товаров", "Провал");
                return null;
            }
        };

        ((BaseApplication) getApplication()).getHttpService().subscribeAction(0, onNextAction, error, null);
    }

    @Override
    public void send(String message) {
        if ("".equals(message)){
            Toast.makeText(this, "Сообщение пустое", Toast.LENGTH_LONG).show();
            return;
        }

        Action1<Object> onNextAction = new Action1<Object>() {
            @Override
            public void call(Object s) {
                if (s != null) {
                    progressDialog.hide();
                    Intent intent = new Intent(MainActivity.this, MsgForumActivity.class);
                    intent.putExtra("image", "message");
                    startActivity(intent);
                    Log.d("Отправка отзыва", s.toString());
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progressDialog.hide();
                Toast.makeText(MainActivity.this, "Ошибка. Отзыв не отправлен", Toast.LENGTH_LONG).show();
                Log.d("Отправка отзыва", "Провал");

                return null;
            }
        };
        Map<String, Object> map = new HashMap<>();
        map.put("Address", SettingsService.getString(this, SettingsService.ADDRESS));
        map.put("Message", message);
        map.put("Author", SettingsService.getString(this, SettingsService.NUMBER));
        progressDialog.show();
        ((BaseApplication) getApplication()).getHttpService().subscribeAction(2, onNextAction, error, map);
    }
}
