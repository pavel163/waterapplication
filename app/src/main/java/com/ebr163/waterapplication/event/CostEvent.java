package com.ebr163.waterapplication.event;

/**
 * Created by Bakht on 03.03.2016.
 */
public class CostEvent extends BaseEvent {

    private boolean flag;

    public CostEvent(Object data, boolean flag) {
        super(data);
        this.flag = flag;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
