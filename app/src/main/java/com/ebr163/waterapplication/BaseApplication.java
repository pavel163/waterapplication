package com.ebr163.waterapplication;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ebr163.waterapplication.service.http.HttpService;
import com.facebook.FacebookSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Bakht on 28.02.2016.
 */
public class BaseApplication extends Application {

    public static final String TAG = BaseApplication.class
            .getSimpleName();

    private HttpService httpService;
    private RequestQueue mRequestQueue;
    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        httpService = HttpService.getInstance();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    public HttpService getHttpService() {
        return httpService;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker("UA-75827645-1");
            mTracker.enableExceptionReporting(true);
            mTracker.enableAutoActivityTracking(true);
            mTracker.enableAdvertisingIdCollection(true);
        }
        return mTracker;
    }
}
