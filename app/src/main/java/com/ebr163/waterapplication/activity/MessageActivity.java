package com.ebr163.waterapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.base.BaseActivity;
import com.ebr163.waterapplication.ui.SmartyImageView;

public class MessageActivity extends BaseActivity implements View.OnClickListener{

    protected TextView message;
    protected SmartyImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        initUI();

    }

    @Override
    protected void initUI() {
        super.initUI();
        message = (TextView) findViewById(R.id.message);
        image = (SmartyImageView) findViewById(R.id.image);
        if ("order".equals(getIntent().getStringExtra("image"))){
            image.setImageResource(R.drawable.ruwater12);
            message.setText("Заказ принят!");
        }
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
