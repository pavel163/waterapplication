package com.ebr163.waterapplication.fragment;

import android.view.View;
import android.widget.Toast;

import com.ebr163.waterapplication.service.SettingsService;

/**
 * Created by Bakht on 24.04.2016.
 */
public class ProfFragment extends ProfileFragment {

    public static ProfFragment newInstance(){
        return new ProfFragment();
    }

    @Override
    public void onClick(View v) {
        SettingsService.savePhone(getActivity(), phone.getEditText().getText().toString());
        SettingsService.saveAddress(getActivity(), address.getEditText().getText().toString());
        Toast.makeText(getActivity(), "Сохранено", Toast.LENGTH_LONG).show();
    }
}
