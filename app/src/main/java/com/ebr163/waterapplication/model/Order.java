package com.ebr163.waterapplication.model;

import android.content.Context;

import com.ebr163.waterapplication.service.SettingsService;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mac1 on 21.03.16.
 */
public class Order {
    public String Address;
    public String Tare;
    public String Day;
    public String Time;
    public String PhoneNumber;
    public Map<String, String> Basket = new HashMap<>();

    public Order(Context context){
        PhoneNumber = SettingsService.getString(context, SettingsService.NUMBER);
        Address = SettingsService.getString(context, SettingsService.ADDRESS);
    }
}
