package com.ebr163.waterapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.ebr163.waterapplication.BaseApplication;
import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.base.BaseActivity;
import com.ebr163.waterapplication.adapter.check.CheckAdapter;
import com.ebr163.waterapplication.fragment.SaleDialog;
import com.ebr163.waterapplication.model.Order;
import com.ebr163.waterapplication.service.SettingsService;
import com.ebr163.waterapplication.ui.SmartyTextView;
import com.ebr163.waterapplication.util.AppHelper;
import com.ebr163.waterapplication.util.DataUtils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.functions.Action1;
import rx.functions.Func1;

public class CheckActivity extends BaseActivity implements View.OnClickListener {

    private CheckBox checkBox;
    private RadioButton todayBtn;
    private RadioButton tommorow;
    private RadioButton aftertTommorow;
    private RadioButton time1;
    private RadioButton time2;
    private RadioButton time3;
    private SmartyTextView summ;
    private ProgressDialog progressDialog;
    private Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_two);
        t.setScreenName("Экран проверки заказа");
        t.send(new HitBuilders.ScreenViewBuilder().build());
        if ("".equals(SettingsService.getString(this, SettingsService.FIRST_START))) {
            AppHelper.listProducts.put("300", new Pair<>("Амурская Высшая категория в подарок", 1));
        }
        initUI();
        tracker = ((BaseApplication) getApplication()).getDefaultTracker();
    }

    @Override
    protected void initUI() {
        super.initUI();
        setToolbarTitle("Проверка заказа");
        List<Map<String, Object>> products = new ArrayList<>();
        for (Map.Entry<String, Pair<String, Integer>> entry : AppHelper.listProducts.entrySet()) {
            Map<String, Object> map = new HashMap<>();
            map.put(AppHelper.ID, entry.getKey());
            map.put(AppHelper.NAME, entry.getValue().first);
            map.put(AppHelper.COUNT, entry.getValue().second);
            products.add(map);
        }

        LinearLayout list = (LinearLayout) findViewById(R.id.list);

        LayoutInflater ltInflater = getLayoutInflater();
        for (Map<String, Object> map : products) {
            View view1 = ltInflater.inflate(R.layout.item_check, list, false);
            ((SmartyTextView) view1.findViewById(R.id.name)).setText(map.get(AppHelper.NAME).toString());
            ((SmartyTextView) view1.findViewById(R.id.count)).setText("Количество " + map.get(AppHelper.COUNT).toString());
            list.addView(view1);
        }

//        ListView listView = (ListView) findViewById(R.id.list);
//        CheckAdapter checkAdapter = new CheckAdapter(this, products);
//        listView.setAdapter(checkAdapter);

        checkBox = (CheckBox) findViewById(R.id.tara);

        todayBtn = (RadioButton) findViewById(R.id.today);
        tommorow = (RadioButton) findViewById(R.id.tommorow);
        aftertTommorow = (RadioButton) findViewById(R.id.after_tomorrow);

        todayBtn.setText(DataUtils.getCurrentData());
        tommorow.setText(DataUtils.getTommorow());
        aftertTommorow.setText(DataUtils.getAfterTommorow());

        todayBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    return;
                }

                installSettings();
            }
        });

        tommorow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    return;
                }

                if (todayBtn.getVisibility() == View.VISIBLE) {
                    if (DataUtils.isTommorowWeekend()) {
                        time3.setVisibility(View.VISIBLE);
                        time1.setVisibility(View.GONE);
                        time2.setVisibility(View.GONE);
                    } else {
                        time1.setVisibility(View.VISIBLE);
                        time2.setVisibility(View.VISIBLE);
                        time3.setVisibility(View.GONE);
                    }
                } else {
                    installSettings();
                }
            }
        });

        aftertTommorow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    return;
                }
                time2.setVisibility(View.VISIBLE);
                time1.setVisibility(View.VISIBLE);
                time3.setVisibility(View.GONE);

                if (DataUtils.isAfterTommorowWeekend()) {
                    time1.setVisibility(View.GONE);
                    time2.setVisibility(View.GONE);
                    time3.setVisibility(View.VISIBLE);
                }
            }
        });

        time1 = (RadioButton) findViewById(R.id.time1);
        time2 = (RadioButton) findViewById(R.id.time2);
        time3 = (RadioButton) findViewById(R.id.time3);

        installSettings();

        summ = (SmartyTextView) findViewById(R.id.summ);
        summ.setText("Сумма заказа: " + getIntent().getStringExtra("sum") + " рублей");
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Отправка заказа...");

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == -1) {
            id = ((View) v.getParent()).getId();
        }
        switch (id) {
            case R.id.check_btn:
                if ("".equals(SettingsService.getString(this, SettingsService.ADDRESS))) {
                    Toast.makeText(this, "Для совершения заказа необходимо указать адрес", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(this, ProfActivity.class));
                    return;
                }
                send();

                break;
        }
    }

    private void send() {
        Order order = new Order(this);
        if (AppHelper.listProducts.isEmpty()) {
            Toast.makeText(this, "Нет товаров", Toast.LENGTH_LONG).show();
            return;
        }

        if (todayBtn.isChecked()) {
            order.Day = "0";
        } else if (tommorow.isChecked()) {
            order.Day = "1";
        } else if (aftertTommorow.isChecked()) {
            order.Day = "2";
        } else {
            Toast.makeText(this, "Выберите день", Toast.LENGTH_LONG).show();
            return;
        }

        if (time1.isChecked()) {
            order.Time = "0";
        } else if (time2.isChecked()) {
            order.Time = "1";
        } else if (time3.isChecked()) {
            order.Time = "2";
        } else {
            Toast.makeText(this, "Выберите время", Toast.LENGTH_LONG).show();
            return;
        }

        if (checkBox.isChecked()) {
            order.Tare = "1";
        } else {
            order.Tare = "0";
        }


        for (Map.Entry<String, Pair<String, Integer>> entry : AppHelper.listProducts.entrySet()) {
            order.Basket.put(entry.getKey(), String.valueOf(entry.getValue().second));
        }

        Action1<Object> onNextAction = new Action1<Object>() {
            @Override
            public void call(Object s) {
                if (s != null) {

                    tracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Заказы")
                            .setAction("Сделал заказ " + SettingsService.getString(CheckActivity.this, SettingsService.NUMBER))
                            .setValue(1)
                            .build());

                    for (Map.Entry<String, Pair<String, Integer>> entry : AppHelper.listProducts.entrySet()) {
                        tracker.send(new HitBuilders.EventBuilder()
                                .setCategory(SettingsService.getString(CheckActivity.this, SettingsService.NUMBER) + " заказал: ")
                                .setAction("id продукта " + entry.getKey())
                                .setValue(1)
                                .build());
                    }

                    tracker.send(new HitBuilders.EventBuilder()
                            .setCategory(SettingsService.getString(CheckActivity.this, SettingsService.NUMBER))
                            .setAction(summ.getText().toString())
                            .setValue(1)
                            .build());

                    progressDialog.hide();
                    AppHelper.listProducts.clear();
                    SettingsService.setFirstStart(CheckActivity.this);
                    startMessageActivity();
                    Log.d("Отправка заказа", s.toString());
                }
            }
        };

        Func1 error = new Func1() {
            @Override
            public Object call(Object o) {
                progressDialog.hide();
                Toast.makeText(CheckActivity.this, "Ошибка. Заказ не отправлен", Toast.LENGTH_LONG).show();
                Log.d("Отправка заказа", "Провал");

                return null;
            }
        };
        progressDialog.show();
        ((BaseApplication) getApplication()).getHttpService().subscribeAction(1, onNextAction, error, order);
    }

    private void startMessageActivity() {
        SettingsService.saveState(this, true);
        SettingsService.saveData(this, DataUtils.getCurrentDayAndMonth());
        Intent intent = new Intent(CheckActivity.this, MessageActivity.class);
        intent.putExtra("image", "order");
        startActivity(intent);
    }

    private void installSettings() {
        if (DataUtils.isWeekend()) {
            if (DataUtils.getCurrentTime() >= 8) {
                todayBtn.setVisibility(View.GONE);
                if (DataUtils.isTommorowWeekend()) {
                    time3.setVisibility(View.VISIBLE);
                    time1.setVisibility(View.GONE);
                    time2.setVisibility(View.GONE);
                } else {
                    if (DataUtils.getCurrentTime() >= 20 || DataUtils.getCurrentTime() == 19 && DataUtils.getCurrentMinute() > 50) {
                        time1.setVisibility(View.GONE);
                    } else {
                        time1.setVisibility(View.VISIBLE);
                    }
                    time3.setVisibility(View.GONE);
                    time2.setVisibility(View.VISIBLE);
                }
            } else {
                todayBtn.setVisibility(View.VISIBLE);
                time1.setVisibility(View.GONE);
                time3.setVisibility(View.VISIBLE);
                time2.setVisibility(View.GONE);

            }
        } else {
            time1.setVisibility(View.GONE);
            time3.setVisibility(View.GONE);
            time2.setVisibility(View.VISIBLE);
            if (DataUtils.getCurrentTime() < 14) {
                if (DataUtils.getCurrentTime() < 8){
                    todayBtn.setVisibility(View.GONE);
                } else {
                    todayBtn.setVisibility(View.VISIBLE);
                    time2.setVisibility(View.VISIBLE);
                    time3.setVisibility(View.GONE);
                }
                tommorow.setVisibility(View.VISIBLE);
            } else if (DataUtils.getCurrentTime() >= 14 && DataUtils.getCurrentTime() < 20) {
                todayBtn.setVisibility(View.GONE);
                if (DataUtils.isTommorowWeekend()) {
                    time3.setVisibility(View.VISIBLE);
                    time2.setVisibility(View.GONE);
                    time1.setVisibility(View.GONE);
                } else {
                    time1.setVisibility(View.VISIBLE);
                    time2.setVisibility(View.VISIBLE);
                    time3.setVisibility(View.GONE);
                }
                if (DataUtils.getCurrentTime() == 19 && DataUtils.getCurrentMinute() > 50) {
                    if (DataUtils.isTommorowWeekend()) {
                        tommorow.setVisibility(View.VISIBLE);
                        time3.setVisibility(View.VISIBLE);
                    } else {
                        time1.setVisibility(View.GONE);
                    }
                }
            } else if (DataUtils.getCurrentTime() >= 20) {
                time1.setVisibility(View.GONE);
                todayBtn.setVisibility(View.GONE);
                if (DataUtils.isTommorowWeekend()) {
                    time2.setVisibility(View.GONE);
                    time3.setVisibility(View.VISIBLE);
                    tommorow.setVisibility(View.VISIBLE);
                } else {
                    time2.setVisibility(View.VISIBLE);
                    time3.setVisibility(View.GONE);
                    tommorow.setVisibility(View.VISIBLE);
                }
            } else {
                tommorow.setVisibility(View.GONE);
                todayBtn.setVisibility(View.GONE);
                if (!DataUtils.isAfterTommorowWeekend()) {
                    time1.setVisibility(View.VISIBLE);
                    time2.setVisibility(View.VISIBLE);
                    time3.setVisibility(View.GONE);
                } else {
                    time1.setVisibility(View.GONE);
                    time2.setVisibility(View.GONE);
                    time3.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
