package com.ebr163.waterapplication.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ebr163.waterapplication.R;

public class MsgForumActivity extends MessageActivity {

    @Override
    protected void initUI() {
        super.initUI();
        message.setText("Спасибо за отзыв! \n" +
                " С Вами мы будем \n" +
                " становиться лучше!");
        image.setImageResource(R.drawable.ok);
    }
}
