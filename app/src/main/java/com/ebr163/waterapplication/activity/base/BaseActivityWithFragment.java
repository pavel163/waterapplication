package com.ebr163.waterapplication.activity.base;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.ebr163.waterapplication.fragment.base.BaseFragment;

/**
 * Created by Bakht on 24.02.2016.
 */
public abstract class BaseActivityWithFragment extends BaseActivity {

    protected BaseFragment fragment;
    protected FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
    }
}
