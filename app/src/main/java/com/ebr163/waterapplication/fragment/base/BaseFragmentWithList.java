package com.ebr163.waterapplication.fragment.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ebr163.waterapplication.R;

/**
 * Created by Bakht on 28.02.2016.
 */
public abstract class BaseFragmentWithList extends BaseFragment implements View.OnClickListener{

    protected RecyclerView recyclerView;
    protected TextView emptyListTxt;
    protected Button updateBtn;
    protected ProgressBar progressBar;
    protected OnSetObjectListener objectListener;
    private LinearLayout error;

    public interface OnSetObjectListener {
        void setProducts();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    protected void initUI(View view) {
        initList(view);
    }

    private void initList(View view){
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        emptyListTxt = (TextView) view.findViewById(R.id.empty_list);
        error = (LinearLayout) view.findViewById(R.id.error);
        updateBtn = (Button) view.findViewById(R.id.update_btn);
        updateBtn.setOnClickListener(this);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
    }

    public void setVisibleProgress(int visibility){
        progressBar.setVisibility(visibility);
    }

    public void setVisibleEmptyView(int visibility){
        emptyListTxt.setVisibility(visibility);
        updateBtn.setVisibility(visibility);
    }

    public void setupRecyclerView(){
        setVisibleProgress(View.GONE);
        error.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.update_btn){
            objectListener.setProducts();
        }
    }
}
