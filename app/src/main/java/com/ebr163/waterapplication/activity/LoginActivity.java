package com.ebr163.waterapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.base.BaseActivity;
import com.ebr163.waterapplication.service.SettingsService;
import com.google.android.gms.analytics.HitBuilders;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private TextInputLayout numberEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!SettingsService.getString(this, SettingsService.NUMBER).equals("")){
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        setContentView(R.layout.activity_login);
        t.setScreenName("Логинка");
        t.send(new HitBuilders.ScreenViewBuilder().build());
        initUI();
    }

    @Override
    protected void initUI() {
        super.initUI();
        numberEdt = (TextInputLayout)findViewById(R.id.edt_client);
    }

    @Override
    public void onClick(View v) {
        numberEdt.setErrorEnabled(false);
        String number = numberEdt.getEditText().getText().toString().trim();
        if (number.equals("")){
            numberEdt.setErrorEnabled(true);
            numberEdt.setError("Поле пустое");
        } else {
            SettingsService.savePhone(this, number);
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }
}
