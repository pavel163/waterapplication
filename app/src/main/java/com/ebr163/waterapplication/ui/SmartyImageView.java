package com.ebr163.waterapplication.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.ebr163.waterapplication.R;
import com.squareup.picasso.Picasso;


/**
 * Created by Bakht on 21.02.2016.
 */
public class SmartyImageView extends ImageView {

    public SmartyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setImage(String url){
        Picasso.with(getContext())
                .load(url)
                .placeholder(R.drawable.ruwater11)
                .into(this);
    }

    public void setImage(Drawable drawable){
        setImageDrawable(drawable);
    }

    public void setImage(int res){
        setImageResource(res);
    }
}
