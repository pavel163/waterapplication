package com.ebr163.waterapplication.service.http;

import com.ebr163.waterapplication.model.Order;
import com.ebr163.waterapplication.model.Product;

import java.util.List;
import java.util.Map;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Bakht on 28.02.2016.
 */
public interface HttpApi {

    @GET("/apanel/Ajax/goods")
    Observable<List<Product>> getProducts();

    @Headers({"Content-Type: application/json",
            "charset: UTF-8"})
    @POST("/apanel/Ajax/addOrder")
    Observable<Object> sendOrder(@Body Order order);

    @Headers({"Content-Type: application/json",
            "charset: UTF-8"})
    @POST("/apanel/Ajax/addreview")
    Observable<Object> sendMessage(@Body Map<String, Object> message);
}