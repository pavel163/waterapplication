package com.ebr163.waterapplication.service.http;

import android.support.annotation.NonNull;
import android.util.Log;

import com.ebr163.waterapplication.model.Order;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Bakht on 28.02.2016.
 */
public class HttpService {

    private HttpApi httpApi;

    private static HttpService instance;

    public static HttpService getInstance() {
        if (instance == null) {
            synchronized (HttpService.class) {
                if (instance == null) {
                    instance = new HttpService();
                }
            }
        }
        return instance;
    }

    private HttpService() {
//        OkHttpClient client = new OkHttpClient();
//        client.setConnectTimeout(10, TimeUnit.SECONDS);
//        client.setWriteTimeout(10, TimeUnit.SECONDS);
//        client.setReadTimeout(10, TimeUnit.SECONDS);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://h90940.s04.test-hf.ru")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        httpApi = retrofit.create(HttpApi.class);
    }

    public void subscribeAction(@NonNull int type, Action1 action, Func1 error, Object query) {
        Observable observable = null;
        switch (type) {
            case 0:
                observable = httpApi.getProducts();
                break;
            case 1:
                observable = httpApi.sendOrder((Order) query);
                break;
            case 2:
                observable = httpApi.sendMessage((Map<String, Object>) query);
                break;
        }

        if (observable != null) {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorResumeNext(error)
                    .subscribe(action, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            Log.e("httpService", "Ошибка");
                        }
                    });
        }
    }
}
