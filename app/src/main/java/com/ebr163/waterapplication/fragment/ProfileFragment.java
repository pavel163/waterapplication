package com.ebr163.waterapplication.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.fragment.base.BaseFragment;
import com.ebr163.waterapplication.service.SettingsService;
import com.google.android.gms.analytics.HitBuilders;

/**
 * Created by Bakht on 29.02.2016.
 */
public class ProfileFragment extends BaseFragment implements View.OnClickListener {

    protected Button save;
    protected TextInputLayout phone;
    protected TextInputLayout address;

    public static ProfileFragment newInstance(){
        ProfileFragment profileFragment = new ProfileFragment();
        return profileFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayout(R.layout.fragment_profile);
        t.setScreenName("Экран профиль");
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    protected void initUI(View view){
        save = (Button) view.findViewById(R.id.save);
        save.setOnClickListener(this);
        phone = (TextInputLayout) view.findViewById(R.id.tel);
        phone.getEditText().setText(SettingsService.getString(getActivity(), SettingsService.NUMBER));
        address = (TextInputLayout) view.findViewById(R.id.adress);
        address.getEditText().setText(SettingsService.getString(getActivity(), SettingsService.ADDRESS));
    }

    @Override
    public void onClick(View v) {
        SettingsService.savePhone(getActivity(), phone.getEditText().getText().toString());
        SettingsService.saveAddress(getActivity(), address.getEditText().getText().toString());
        Toast.makeText(getActivity(), "Сохранено", Toast.LENGTH_LONG).show();
        getActivity().onBackPressed();
    }
}
