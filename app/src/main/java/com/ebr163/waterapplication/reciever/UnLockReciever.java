package com.ebr163.waterapplication.reciever;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Pair;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.LoginActivity;
import com.ebr163.waterapplication.service.SettingsService;
import com.ebr163.waterapplication.util.DataUtils;

public class UnLockReciever extends BroadcastReceiver {

    private NotificationManager notificationManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        Pair<Integer, Integer> data = SettingsService.getData(context);
        Pair<Integer, Integer> currentData = DataUtils.getCurrentDayAndMonth();
        int counter;
        if (data.second == currentData.second) {
            counter = currentData.first - data.first;
        } else {
            counter = 30 - data.first + currentData.first;
        }

        if (SettingsService.getState(context) && counter >= 7) {
            SettingsService.saveState(context, false);
            SettingsService.saveCounter(context, 1);
            notifyMessage(context);
            SettingsService.saveData(context, currentData);
        } else if (SettingsService.getState(context) && counter < 7) {
            SettingsService.saveCounter(context, counter);
        } else if (!SettingsService.getState(context) && counter >= 5) {
            SettingsService.saveCounter(context, 1);
            notifyMessage(context);
            SettingsService.saveData(context, currentData);
        } else {
            SettingsService.saveCounter(context, counter);
        }


    }

    private void notifyMessage(Context context) {
        notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, createNotification(context, "Не пора ли заказать водичку?"));
    }

    private Notification createNotification(Context context, String message) {
        Intent intent = new Intent(context, LoginActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
        return new NotificationCompat.Builder(context)
                .setContentTitle("RU.WATER")
                .setContentText(message)
                .setSmallIcon(R.mipmap.notify_small)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.logo))
                .setContentIntent(pIntent)
                .build();
    }
}
