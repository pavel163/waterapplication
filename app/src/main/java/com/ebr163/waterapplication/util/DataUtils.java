package com.ebr163.waterapplication.util;

import android.util.Pair;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Bakht on 05.04.2016.
 */
public class DataUtils {

    public static String getDayOfWeek() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        return sdf.format(d);
    }

    public static boolean isWeekend() {
        if (getDayOfWeek().equals("воскресенье") || getDayOfWeek().equals("суббота")) {
            return true;
        }
        return false;
    }

    public static boolean isTommorowWeekend() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String day = sdf.format(c.getTime());
        if (day.equals("воскресенье") || day.equals("суббота")) {
            return true;
        }
        return false;
    }

    public static boolean isAfterTommorowWeekend() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 2);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String day = sdf.format(c.getTime());
        if (day.equals("воскресенье") || day.equals("суббота")) {
            return true;
        }
        return false;
    }

    public static String getCurrentData() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd.MMM.yyyy");
        return df.format(c.getTime());
    }

    public static Pair<Integer, Integer> getCurrentDayAndMonth() {
        Calendar c = Calendar.getInstance();
        return new Pair<>(c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.MONTH));
    }

    public static int getCurrentTime() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.HOUR_OF_DAY);
    }

    public static int getCurrentMinute() {
        Calendar c = Calendar.getInstance();
        return c.get(Calendar.MINUTE);
    }

    public static String getTommorow() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        SimpleDateFormat df = new SimpleDateFormat("dd.MMM.yyyy");
        return df.format(c.getTime());
    }

    public static String getAfterTommorow() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 2);
        SimpleDateFormat df = new SimpleDateFormat("dd.MMM.yyyy");
        return df.format(c.getTime());
    }
}
