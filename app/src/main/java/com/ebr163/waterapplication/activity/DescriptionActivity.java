package com.ebr163.waterapplication.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ebr163.waterapplication.BaseApplication;
import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.base.BaseActivity;
import com.ebr163.waterapplication.ui.SmartyImageView;
import com.ebr163.waterapplication.ui.SmartyTextView;

public class DescriptionActivity extends BaseActivity {

    private String id;
    private String name;
    private String image;
    private SmartyTextView description;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        id = getIntent().getStringExtra("id");
        name = getIntent().getStringExtra("name");
        image = getIntent().getStringExtra("image");

        initUI();
    }

    @Override
    protected void initUI() {
        super.initUI();
        SmartyImageView imageView = (SmartyImageView) findViewById(R.id.image);
        imageView.setImage(image);
        description = (SmartyTextView) findViewById(R.id.description);
        SmartyTextView name = (SmartyTextView) findViewById(R.id.name);
        name.setText(this.name);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        getDescription();
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        setToolbarTitle("Товары");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_24dp));
    }

    private void getDescription() {
        String url = "http://h90940.s04.test-hf.ru/apanel/Ajax/getDescription/" + id;
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.replace("<p>", "");
                response = response.replace("</p>", "");
                description.setText(response);
                progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        ((BaseApplication) getApplicationContext()).addToRequestQueue(request);
    }
}