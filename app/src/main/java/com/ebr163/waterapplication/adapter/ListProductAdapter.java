package com.ebr163.waterapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.model.Product;

import java.util.List;

/**
 * Created by Bakht on 28.02.2016.
 */
public class ListProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Product> products;

    public ListProductAdapter (List<Product> products){
        this.products = products;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return ListProductHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListProductHolder viewHolder = (ListProductHolder) holder;
        Product product = this.products.get(position);
        viewHolder.setImage(product.Image);
        viewHolder.setName(product.Title);
        viewHolder.setCost(product.Cost);
        viewHolder.setId(product.Id);
    }

    @Override
    public int getItemCount() {
        return products == null ? 0 : products.size();
    }
}
