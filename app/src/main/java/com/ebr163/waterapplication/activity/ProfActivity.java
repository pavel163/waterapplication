package com.ebr163.waterapplication.activity;

import android.os.Bundle;

import com.ebr163.waterapplication.R;
import com.ebr163.waterapplication.activity.base.BaseActivityWithFragment;
import com.ebr163.waterapplication.fragment.ProfileFragment;

public class ProfActivity extends BaseActivityWithFragment {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);
        fragment = ProfileFragment.newInstance();
        fragmentManager.beginTransaction()
                .replace(R.id.main_fragment_container, fragment, "")
                .commit();
        initToolbar();
        setToolbarTitle("Профиль");
    }
}
