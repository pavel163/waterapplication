package com.ebr163.waterapplication.service;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Pair;

/**
 * Created by Bakht on 27.02.2016.
 */
public class SettingsService {

    private static final String SETTINGS = "settings";

    public static final String NUMBER = "number";
    public static final String ADDRESS = "address";
    public static final String FIRST_START = "first_start";

    public static void savePhone(Context context, String number) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(NUMBER, number);
        editor.apply();
    }

    public static void saveAddress(Context context, String address) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ADDRESS, address);
        editor.apply();
    }

    public static void setFirstStart(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(FIRST_START, "ok");
        editor.apply();
    }

    public static String getString(Context context, String key){
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public static void saveData(Context context, Pair<Integer, Integer> data){
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("day", data.first);
        editor.putInt("month", data.second);
        editor.apply();
    }

    public static Pair<Integer, Integer> getData(Context context){
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        Pair<Integer, Integer> data = new Pair<>(preferences.getInt("day", 0), preferences.getInt("month", 0));
        return data;
    }

    public static void saveCounter(Context context, int counter) {
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("counter", counter);
        editor.apply();
    }

    public static int getCounter(Context context){
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        return preferences.getInt("counter", 0);
    }

    public static void saveState(Context context, boolean state){
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("state", state);
        editor.apply();
    }

    public static boolean getState(Context context){
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        return preferences.getBoolean("state", true);
    }
}
